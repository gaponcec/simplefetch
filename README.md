# simplefetch

A simple program similar to neofetch and others
Still under development, not for use in a serious way

## How to run
Just execute ./simplefetch

### Customize
Simplefetch script just gets the information gathered by the script
`gather_values.sh`.

If you whish to add more fields or change the way values are gathered, you need
to go to `gather_values.sh` and change the commands that are being run there.

If you whish to add more information, you can update the `sf_labels.conf` and
the `sf_values.conf`.
The `sf_labels.conf` file has the Labels to use in the simplefetch:
Procesador:
Memoria:
Distro:
Paquetes:
Kernel:
Shell:
Uptime:

You can change the order or the string if you want to, but bear in mind that If
you change the order you will also have to change the `sf_values.conf`, as this
other file has the information that will be displayed.
You can also add new fields, you will just have to add a new member in labels,
values and a way to gather it.

### Supported distros.

Simplefetch is currently able to display the logos of these distros:
- Arch
- Artix
- Debian
- Fedora
- Sparkylinux
- Ubuntu
