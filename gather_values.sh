#!/bin/bash

. ./.sf_variables

######## Get local, if not spanish, then default is english ###################
lang="en"
if [[ "$LANG" == *"es"* ]]
then
  lang="es"
fi
###############################################################################

###### Change string for grepping processor model according to language #######
grep_proc="Model name:"
if [[ $lang == "es" ]]
then
  grep_proc="Nombre del modelo:"
fi
###############################################################################

############# Find out package manager and number of packages #################
packages="No package manager found"
which apt &>/dev/null
if [[ $? -eq 0 ]]
then
  pkg_kind="debs"
  packages=$(apt list --installed 2>/dev/null|wc -l)
fi
which pacman &>/dev/null
if [[ $? -eq 0 ]]
then
  pkg_kind="pkgs"
  packages=$(pacman -Q 2>/dev/null|wc -l)
fi
which dnf &>/dev/null
if [[ $? -eq 0 ]]
then
  pkg_kind="rmps"
  packages=$(($(dnf list --installed 2>/dev/null|wc -l)-1))
fi
###############################################################################

proc=$(lscpu |grep "$grep_proc"|awk -F":" '{print $2}'|sed 's/^\ \ *//')
mem=$(free -h|grep Mem|awk '{print $3,"/",$2}')
distro=$(grep PRETTY_NAME /etc/os-release|cut -d\= -f2|sed 's/"//g')

############################### Select Logos ##################################
if [[ "$distro" == *"Sparky"* ]]
then
  art="logos/sparky.txt"
elif [[ "$distro" == *"Artix"* ]]
then
  art="logos/artix.txt"
elif [[ "$distro" == *"Fedora"* ]]
then
  art="logos/fedora.txt"
elif [[ "$distro" == *"Debian"* ]]
then
  art="logos/debian.txt"
elif [[ "$distro" == *"Ubuntu"* ]]
then
  art="logos/ubuntu.txt"
elif [[ "$distro" == *"Arch"* ]]
then
  art="logos/arch.txt"
else
  art="no_art"
fi
###############################################################################
kernel=$(uname -sr)
uptm=$(uptime -p|sed 's/up //')
shell=$(basename $SHELL)

paks="${NoColor}$packages"
if [[ packages != "No package manager found" ]]
then
  paks="$paks${RED}($pkg_kind)"
fi

which flatpak &>/dev/null
if [[ $? -eq 0 ]];
then
  paks="$paks / ${NoColor}$(($(flatpak list|wc -l)-1))${RED}(flatpaks)"
fi
